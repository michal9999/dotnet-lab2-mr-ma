﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {


        static void Main(string[] args)
        {
            int[,] swiat = new int[10, 10];
            Random rnd = new Random();

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    swiat[i, j] = rnd.Next(2);
                }
            }


           
            Gra gra = new Gra(swiat);
            gra.start();
        }

    }

    public class Gra
    {
        private int[,] swiat2;
        public Gra(int[,] swiat)
        {
            swiat2 = new int[swiat.GetLength(0), swiat.GetLength(1)];
            Array.Copy(swiat, swiat2, swiat.Length);
        }

        public int sasiedzi(int x, int y)
        {
            int licznik = 0;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (i == 0 && j == 0) ;
                    else if (x + i < 0 || y + j < 0) ;
                    else if (x + i > 9 || y + j > 9) ;
                    else if (swiat2[x + i, y + j] == 1)
                        licznik++;
                }
            }
            return licznik;
        }

        public void start()
        {
            int[,] kopia = new int[swiat2.GetLength(0), swiat2.GetLength(1)];
            while (true)
            {
                for (int i = 0; i < swiat2.GetLength(0); i++)
                {
                    for (int j = 0; j < swiat2.GetLength(1); j++)
                    {
                        int liczbaSasiadow = sasiedzi(i, j);
                        if (swiat2[i, j] == 1)
                        {
                            if (liczbaSasiadow == 2 || liczbaSasiadow == 3)
                                kopia[i, j] = 1;
                            else
                                kopia[i, j] = 0;
                        }
                        else if (liczbaSasiadow == 3)
                            kopia[i, j] = 1;
                    }
                }

                Array.Copy(kopia, swiat2, kopia.Length);
                for (int i = 0; i < 10; i++)
                {
                    
                    for (int j = 0; j < 10; j++)
                    {
                        Console.SetCursorPosition(i, j);
                        Console.Write(swiat2[i , j] );
                    }

                    
                   // Console.WriteLine();
                   
                }
                Console.WriteLine();
                Console.Write("---------------");
                Console.ReadKey();
            }
        }
    }
}